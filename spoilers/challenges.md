# Challenges

The progression is fairly linear up to the point where you get access
to the Spacetime Junction, at which point it branches out with a
number of "failed timelines" that you explore, and then reset back to
the Junction point till you know enough to get to LHS 451 and finish
the game.

There will be coding challenges. They can't be too contrived. Few of
them should involve writing from scratch. In real life it's much more
common to scrounge together solutions from existing snippets you find.

We can structure it around progressing from language to language. Obviously you
start with Lua, and the Sol/Tana worlds should all use the Orb OS, which is
written in Lua.

## Decrypt journals (Lua)

This is the first challenge; you write the solution in Lua. Nari gives you a
fair bit of hints if you go for too long without solving it.

It confirms that you are a machine consciousness, introduces the character of
Traxus, and indicates that you are connected to him.

## Get off travel blacklist (SQL)

## Mess with portal code (Lisp)

## Light curve analysis (Lily?)

## Reactivate mining robot (Forth)

## Add password logger to compromised account (Moonscript?)

## Password dictionary attack (???)

## Buffer overflow attack (???)
